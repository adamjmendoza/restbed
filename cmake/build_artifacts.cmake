# Copyright (c) 2013, 2014 Corvusoft

set( FRAMEWORK_ARTIFACTS
     ${SOURCE_DIR}/mode
     ${SOURCE_DIR}/mode.h
     ${SOURCE_DIR}/settings
     ${SOURCE_DIR}/settings.h
     ${SOURCE_DIR}/log_level
     ${SOURCE_DIR}/log_level.h
     ${SOURCE_DIR}/status_code
     ${SOURCE_DIR}/status_code.h
     ${SOURCE_DIR}/method
     ${SOURCE_DIR}/method.h
     ${SOURCE_DIR}/resource
     ${SOURCE_DIR}/resource.h
     ${SOURCE_DIR}/request
     ${SOURCE_DIR}/request.h
     ${SOURCE_DIR}/response
     ${SOURCE_DIR}/response.h
     ${SOURCE_DIR}/service
     ${SOURCE_DIR}/service.h
     ${SOURCE_DIR}/logger
     ${SOURCE_DIR}/logger.h
     ${SOURCE_DIR}/log_level
     ${SOURCE_DIR}/log_level.h
)
